package com.in28minutes.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class MyAssertTest {
	
	List<String> todos = Arrays.asList("AWS", "Azure", "DevOps");
	
	@Test
	void test() {
		//assertNull, assertNotNull
		assertEquals(true, todos.contains("AWS"));
		assertTrue(todos.contains("Azure"));
		assertEquals(3, todos.size(), "Failing as Size is different!");
		assertArrayEquals(new int[] {1,2}, new int[] {1,2});
		assertFalse(todos.contains("GCP"));
	}
}

